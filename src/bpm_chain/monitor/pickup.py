#############################
## PickUp signal generator ##
#############################

import numpy as np
from scipy.signal import bilinear
from scipy.signal import lfilter


class ConfigBunch:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.intensity = 1e10
        self.sigma=262.5e-12
        self.x_max = 5e-3
        self.int_max = 3e11
        self.int_min = 1e9
    def set_x(self, x):
        self.x = x
    def set_y(self, y):
        self.y = y
    def set_intensity(self, intensity):
        self.intensity = intensity
    def set_sigma(self, sigma):
        self.sigma = sigma
    def set_bunch(self, x, y, intensity):
        self.x = x
        self.y = y
        self.intensity = intensity
    def set_int_max(self, int_max):
        self.int_max = int_max
    def set_x_max(self, x_max):
        self.x_max = x_max
    def set_int_min(self, int_min):
        self.int_min = int_min
    def show(self):
        print(f"Bunch Configuration: \
              \n\t bunch position x (x) = {self.x*1e3} mm,\
              \n\t bunch position y (y) = {self.y*1e3} mm,\
              \n\t bunch intensity (intensity) = {self.intensity/1e10}e10 cpb,\
              \n\t bunch sigma length (sigma) = {self.sigma*1e12} ps,\
              \n\t maximum bunch displacement (x_max) = {self.x_max*1e3} mm,\
              \n\t maximum bunch intensity (int_max) = {self.int_max/1e10}e10 ppb,\
              \n\t minimum bunch intensity (int_min) = {self.int_min/1e10}e10 ppb")

class ConfigPickup:
    def __init__(self):
        self.lin = False
        self.type = 'stripline'
        self.source = 'analytic'
        self.r_button = None
        self.r_pipe= None
        self.l=None
        self.z0=None
        self.t_button=None
        self.w_gap=None
        self.corr=None
        self.step=1e-12
        self.t0=2e-9
        self.T=12e-9
    def set_lin(self, lin):
        self.lin = lin
    def set_pu(self, pu):
        self.pu = pu
    def set_source(self, source):
        self.source = source
    def set_time_axis(self, step=1e-12, t0=2e-9, T=12e-9):
        self.step=step
        self.t0=t0
        self.T=T
    def set_r_button(self, r_button):
        self.r_button = r_button
    def set_r_pipe(self, r_pipe):
        self.r_pipe = r_pipe
    def set_button(self, lin=True, r_button=12e-3, r_pipe=24.5e-3, t_button=14e-3, w_gap=0.6e-3, z0=50, corr=1.747):
        self.type = 'button'
        self.r_button = r_button
        self.r_pipe= r_pipe
        self.z0=z0
        self.t_button=t_button
        self.w_gap=w_gap
        self.corr=corr
    def set_stripline(self, lin=True, r_button=6e-3, r_pipe=44.5e-3, l=0.125, z0=50, corr=2.093):
        self.type = 'stripline'
        self.r_button = r_button
        self.r_pipe= r_pipe
        self.z0=z0
        self.l=l        
        self.corr=corr
    def show(self):
        print(f"Pickup Configuration main parameters: \
              \n\t position characteristic linearity (lin) = {self.lin},\
              \n\t type (type) = {self.type},\
              \n\t model (source) = {self.source},\
              \n\t electrode radius (r_button) = {self.r_button*1e3} mm,\
              \n\t pipe radius (r_pipe) = {self.r_pipe*1e3} mm")


# Position Characteristic 

def poschar(x=0,y=0,r_pipe=24.5e-3,r_button=12e-3, lin=False):
    alpha_half = np.divide(r_button,r_pipe)
    if lin:
        electrode = (1/np.pi)*(alpha_half+2*np.sin(alpha_half)*x/r_pipe)
    else:
        phi = np.arctan2(y,x) 
        rad = np.sqrt(np.square(x)+np.square(y))
        numerator1 = (r_pipe + rad)*np.tan((-phi+alpha_half)/2)
        numerator2 = (r_pipe + rad)*np.tan((-phi-alpha_half)/2)
        denominator = (r_pipe - rad)
        electrode = np.transpose(np.array([np.arctan2(numerator1,denominator) - np.arctan2(numerator2,denominator)]))
        electrode[np.where(numerator2>numerator1)] = electrode[np.where(numerator2>numerator1)] + np.pi
        electrode = electrode/np.pi
    return electrode
    

# Generic file signal, the file must contains in the first line the bunch intensity, in the second the radius of the electrode, in the third the radius of the pipe, then time and data.

def filetrace(bunch_c, pickup_c,filename="", maximum=False):
    if not filename:
        filename='../data/monitor/pickup/button/CST.txt'
    file_info = open(filename, "r")
    I0 = float(file_info.readline().split(': ')[1])    
    pickup_c.r_button = float(file_info.readline().split(': ')[1]) 
    pickup_c.r_pipe = float(file_info.readline().split(': ')[1])
    if maximum:
        intensity=bunch_c.int_max
        x=bunch_c.x_max
        y=0
    else:
        intensity=bunch_c.intensity
        x=bunch_c.x
        y=bunch_c.y
    pos00 = poschar(0,0,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    posxA = poschar(x,y,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    posxB = poschar(-x,y,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    posyA = poschar(y,x,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    posyB = poschar(-y,x,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    file_data = np.genfromtxt(filename, skip_header=5)
    signal = (file_data[:,1])*intensity/(I0*pos00)
    time = file_data[:,0]*1e-9
    pu = np.zeros((5,len(signal)))
    pu[1:,:] = np.array([posxA*signal,posxB*signal,posyA*signal,posyB*signal])
    pu[0,:] = time
    return pu

    
# Analytic time signal

def analytic(bunch_c, pickup_c, maximum=False):
    c = 3e8
    charge = 1.602176e-19
    time = np.arange(0, pickup_c.T, pickup_c.step)
    if maximum:
        intensity=bunch_c.int_max
        x=bunch_c.x_max
        y=0
    else:
        intensity=bunch_c.intensity
        x=bunch_c.x
        y=bunch_c.y
    if pickup_c.type=='stripline':
        tau = 2*pickup_c.l/c
        signal = pickup_c.corr*pickup_c.z0*0.5*charge*intensity*\
            (np.sqrt(1/(2*np.pi))/bunch_c.sigma)*\
            (np.exp(-np.square((time-t0)/bunch_c.sigma)/2)\
             -np.exp(-np.square((time-pickup_c.t0-tau)/bunch_c.sigma)/2))
    else:
        eps0 = 8.854e-12
        c_button = np.divide(2*np.pi*eps0,np.log((pickup_c.r_button+pickup_c.w_gap)/pickup_c.r_button))*pickup_c.t_button
        w1 = (pickup_c.z0*c_button)**(-1)
        w2 = np.divide(c,2*pickup_c.r_button)
        coeff = pickup_c.z0*(w1/w2)
        num = [coeff, 0]
        den = [1, w1]
        fs = 1/(2*pickup_c.step)
        buttonFT = bilinear(num,den,fs)
        beam = pickup_c.corr*intensity*charge*\
        (np.sqrt(1/(2*np.pi))/bunch_c.sigma)*np.exp(-np.square((time-pickup_c.t0)/bunch_c.sigma)/2)
        signal = lfilter(buttonFT[0],buttonFT[1],beam)   
    posxA = poschar(x,y,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    posxB = poschar(-x,y,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    posyA = poschar(y,x,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    posyB = poschar(-y,x,pickup_c.r_pipe,pickup_c.r_button,pickup_c.lin)
    pu = np.zeros((5,len(signal)))
    pu[1:,:] = np.array([posxA*signal,posxB*signal,posyA*signal,posyB*signal])
    pu[0,:] = time
    return pu

    
# Top Functions

def pickup(bunch_c, pickup_c, filename="", maximum=False):
    if pickup_c.source == 'file':
        pu = filetrace(bunch_c, pickup_c, filename, maximum)
    else:
        pu = analytic(bunch_c, pickup_c, maximum)
    return pu

