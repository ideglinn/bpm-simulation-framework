# Digital Signal Processing block for the position extraction

import numpy as np
from scipy.stats import mode

# trigger
def bunchDetector(matrix,master, electrodesPerSignal=2, bunchesPerTurn=3):
    dim = np.shape(matrix)
    dim_result = [dim[0],dim[1],dim[2]-len(master)+1]
    correlated_matrix = np.zeros(dim_result)
    for i in range(0,dim[0]):
        for j in range(0,dim[1]):
            correlated_matrix[i,j,:]=np.correlate(matrix[i,j,:],master)
    dummy = np.array(correlated_matrix)
    peaks = np.zeros((bunchesPerTurn*electrodesPerSignal,correlated_matrix.shape[0],correlated_matrix.shape[1]), dtype=int)
    peaks[0,:,:]=np.argmax(correlated_matrix,axis=2)
    for k in range(1,bunchesPerTurn*electrodesPerSignal):
        for i in range(0,dim[0]):
            for j in range(0,dim[1]):
                dummy[i,j,peaks[k-1,i,j]-8:peaks[k-1,i,j]+8]=np.zeros(np.shape(dummy[i,j,peaks[k-1,i,j]-8:peaks[k-1,i,j]+8]))
        peaks[k,:,:]=np.argmax(dummy,axis=2)
    startpoint = np.min(peaks,axis=0)
    # new part
    startmode= mode(startpoint, axis=0) 
    for k in range(0,startpoint.shape[0]):
        if (np.abs(startpoint[k,0]-startmode[0][0][0])>1):
            startpoint[k,0]=startmode[0][0][0]
        if (np.abs(startpoint[k,1]-startmode[0][0][1])>1):
            startpoint[k,1]=startmode[0][0][1]        
    return startpoint   
    
# RMS Algorithm 

def rmsSinglet(signal_i, trigger, combiner_c, bunchOffset = 1, offset = 0, electrodeLen = 13.5e-9):
    ts=signal_i[0,0,1]-signal_i[0,0,0]
    electrodeN= int(electrodeLen/ts)
    HA0   = trigger[:,0]+int(offset+bunchOffset*combiner_c.bunch_dist/ts)
    HAindex = HA0[:,None] + np.arange(electrodeN)
    HB0   = trigger[:,0]+int((offset+bunchOffset*combiner_c.bunch_dist+combiner_c.delayAB)/ts)
    HBindex = HB0[:,None] + np.arange(electrodeN)
    if combiner_c.timeMuxN==4:
        VA0   = trigger[:,0]+int(offset+bunchOffset*combiner_c.bunch_dist+combiner_c.delayHV/ts)
        VAindex = VA0[:,None] + np.arange(electrodeN)
        VB0   = trigger[:,0]+int((offset+bunchOffset*combiner_c.bunch_dist+combiner_c.delayHV+combiner_c.delayAB)/ts)
        VBindex = VB0[:,None] + np.arange(electrodeN)
    else:
        VA0   = trigger[:,1]+int(offset+bunchOffset*combiner_c.bunch_dist/ts)
        VAindex = VA0[:,None] + np.arange(electrodeN)
        VB0   = trigger[:,1]+int((offset+bunchOffset*combiner_c.bunch_dist+combiner_c.delayAB)/ts)
        VBindex = VB0[:,None] + np.arange(electrodeN)
    # RMS Amplitude
    HA = np.sqrt(np.mean(np.square(signal_i[(np.arange(HAindex.shape[0])[:,None],1,HAindex)]), axis=1))
    HB = np.sqrt(np.mean(np.square(signal_i[(np.arange(HBindex.shape[0])[:,None],1,HBindex)]), axis=1))   
    VA = np.sqrt(np.mean(np.square(signal_i[(np.arange(VAindex.shape[0])[:,None],2,VAindex)]), axis=1))
    VB = np.sqrt(np.mean(np.square(signal_i[(np.arange(VBindex.shape[0])[:,None],2,VBindex)]), axis=1))
    # Sigma Delta Normalisation
    Hnorm = np.divide((HA-HB),(HA+HB))
    Vnorm = np.divide((VA-VB),(VA+VB))
    norm = np.array([Hnorm,Vnorm])
    return norm
        

# Normalisation

def norm2mm(norm,r_pipe=40.5e-3,r_button=11.4e-3):
    alpha_half = np.divide(r_button,r_pipe)
    coeff = (r_pipe*2*alpha_half)/(4*np.sin(alpha_half))
    posmm = norm*coeff
    return posmm
  
# Error Evaluation 
  
def errorEval(x,y,pos):
    realPos=np.array([[x],[y]])
    err=(pos-realPos)
    mean=np.mean(err,axis=1)
    sigma=np.std(err,axis=1)
    errorDistribution = np.array([mean,sigma])
    return errorDistribution

# deprecated

def rmsAmp(electrode_i):
    amp=np.sqrt(np.mean(np.square(electrode_i), axis=1))
    return amp
    
def peakAmp(electrode_i):
    amp=np.max(electrode_i, axis=1)
    return amp
    
def sigmaDeltaNorm(ampA, ampB):
    pos = np.divide((ampA-ampB),(ampA+ampB))
    return pos

def peak(signal_i):
    halfbunch = int(np.floor((signal_i.shape[2])/2))
    HA=peakAmp(signal_i[:,1,0:halfbunch])
    HB=peakAmp(signal_i[:,1,halfbunch:halfbunch*2])   
    VA=peakAmp(signal_i[:,2,0:halfbunch])
    VB=peakAmp(signal_i[:,2,halfbunch:halfbunch*2])
    Hnorm=sigmaDeltaNorm(HA,HB)
    Vnorm=sigmaDeltaNorm(VA,VB)
    norm=np.array([Hnorm,Vnorm])
    return norm
    
def rms(signal_i):
    halfbunch = int(np.floor((signal_i.shape[2])/2))
    # RMS amplitude
    HA = np.sqrt(np.mean(np.square(signal_i[:,1,0:halfbunch]), axis=1))
    HB = np.sqrt(np.mean(np.square(signal_i[:,1,halfbunch:halfbunch*2]), axis=1))   
    VA = np.sqrt(np.mean(np.square(signal_i[:,2,0:halfbunch]), axis=1))
    VB = np.sqrt(np.mean(np.square(signal_i[:,2,halfbunch:halfbunch*2]), axis=1))
    # Sigma Delta Normalisation
    Hnorm = np.divide((HA-HB),(HA+HB))
    Vnorm = np.divide((VA-VB),(VA+VB))
    norm = np.array([Hnorm,Vnorm])
    return norm
    
def norm2mmQuad(norm,r_pipe=40.5e-3,r_button=11.4e-3):
    alpha_half = np.divide(r_button,r_pipe)
    coeff = r_button/np.sin(alpha_half)
    posmm = norm*coeff
    return posmm
    
    
def rmsQuad(signal_i):
    halfbunch = int(np.floor((signal_i.shape[2])/2))
    HA=rmsAmp(signal_i[:,1,0:halfbunch])
    HB=rmsAmp(signal_i[:,1,halfbunch:halfbunch*2])   
    VA=rmsAmp(signal_i[:,2,0:halfbunch])
    VB=rmsAmp(signal_i[:,2,halfbunch:halfbunch*2])
    SumAll = HA + HB + VA +VB
    DeltaH = HA - HB
    DeltaV = VA - VB
    Hnorm = np.divide(DeltaH,SumAll)
    Vnorm = np.divide(DeltaV,SumAll)
    norm=np.array([Hnorm,Vnorm])
    return norm
