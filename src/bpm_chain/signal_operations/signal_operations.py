# Generic Operations on signals

import numpy as np
from scipy.optimize import curve_fit
    
def extend(pu, sigLen=25e-9):
    lenght = pu.shape[1]
    if sigLen < pu[0,lenght-1]:
        ts = pu[0,1]-pu[0,0]
        sigLenIndex=int(sigLen/ts)
        signal = pu[1:,0:sigLenIndex]
        time = pu[0,0:sigLenIndex]
    else:
        ts=pu[0,1]-pu[0,0]
        b=(np.log(pu[1:,lenght-2]/pu[1:,lenght-1]))/ts
        b[np.isnan(b)] = 0
        xx = np.arange(pu[0,lenght-1], sigLen, ts)
        yy = np.zeros((pu.shape[0]-1,len(xx)))
        for i in range(0,yy.shape[0]):
            yy[i,:] = pu[i+1,lenght-2]*np.exp(-(xx-pu[0,lenght-2])*b[i])
        signal = np.concatenate((pu[1:,0:lenght-1],yy[:,:]), axis=1)
        time = np.concatenate((pu[0,0:lenght-1],xx))
    ext_pu = np.zeros((pu.shape[0],signal.shape[1]))
    ext_pu[1:,:] = signal
    ext_pu[0,:] = time
    return ext_pu
    
def delay(pu, tDelay=2e-9):
    time = pu[0,:]
    signal = np.array(pu[1:,:])
    ts = time[1]-time[0]
    binsToRoll = int(int(tDelay/ts))
    signal = np.roll(signal,binsToRoll,axis=1)
    delayed_pu = np.zeros(pu.shape)
    delayed_pu[1:,:] = signal
    delayed_pu[0,:] = time
    return delayed_pu

# Multi bunch 
   
def multiBunch(bunch, bunchNum=3):
    multiBunch = np.zeros((bunchNum,bunch.shape[0],bunch.shape[1]))
    for i in range(0,bunch.shape[0]):
        multiBunch[:,i,:] = np.array(bunch[i,:])
    return multiBunch
    
def flatTrain(multiBunch):
    flatT = np.stack(multiBunch,axis=1)
    flatT=np.reshape(flatT,(3,multiBunch.shape[2]*multiBunch.shape[0]))
    return flatT    
    
# Frequency Domain   
  
def PUfft(pu, interp=1):
    t=np.array(pu[0,:])
    signal_i = np.array(pu[1:,:])
    N = signal_i.shape[1]*interp
    Nfreq = int(np.ceil((N+1)*0.5))
    fs=1/(t[1]-t[0])
    signal_o = np.fft.rfft(signal_i, n=N, axis=1)
    if N%2:
        freq=np.linspace(0,(fs/2*(N-1)/N),Nfreq, endpoint=True)
    else:
        freq=np.linspace(0,fs/2,Nfreq, endpoint=True)
    pu_spectra = np.zeros((pu.shape[0],signal_o.shape[1]))*1j
    pu_spectra[0,:]=freq
    pu_spectra[1:,:]=signal_o
    return pu_spectra

def PUfftTrain(pu, interp=1):
    t=np.array(pu[0,0,:])
    signal_i = np.array(pu[:,1:,:])
    N = signal_i.shape[2]*interp
    Nfreq = int(np.ceil((N+1)*0.5))
    fs=1/(t[1]-t[0])
    signal_o = np.fft.rfft(signal_i, n=N, axis=2)
    if N%2:
        freq=np.linspace(0,(fs/2*(N-1)/N),Nfreq, endpoint=True)
    else:
        freq=np.linspace(0,fs/2,Nfreq, endpoint=True)
    pu_spectra = np.zeros((pu.shape[0],pu.shape[1],signal_o.shape[2]))*1j
    pu_spectra[:,0,:]=freq
    pu_spectra[:,1:,:]=signal_o
    return pu_spectra

def PUinterp(pu, interp=1):
    t=np.array(pu[0,0,:])
    ts=t[1]-t[0]
    signal_i = np.array(pu[:,1:,:])
    N = signal_i.shape[2]*interp
    signal_o = np.fft.irfft(np.fft.rfft(signal_i,  axis=2), n=N, axis=2)*interp
    t=np.arange(t[0],t[0]+ts/interp*N,ts/interp)[0:N]
    pu_interp = np.zeros((pu.shape[0],pu.shape[1],signal_o.shape[2]))
    pu_interp[:,0,:]=t
    pu_interp[:,1:,:]=signal_o
    return pu_interp    