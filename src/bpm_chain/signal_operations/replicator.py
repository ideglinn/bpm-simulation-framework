# Replicator Block
import numpy as np

# class ConfigReplicator:
    # def __init__(self):
        # self.replica_num = 500
    # def set_replica_num(self, replica_num):
        # self.replica_num = replica_num
    # def show(self):
        # print(f"Replicator Configuration:\
                # \n\t Number of replicas (replica_num) = {self.replica_num},")

def replicator(signal_i, replica_num=500):
    replicas = np.zeros((replica_num,signal_i.shape[0],signal_i.shape[1]))
    for i in range(0,signal_i.shape[0]):
        replicas[:,i,:] = np.array(signal_i[i,:])
    return replicas