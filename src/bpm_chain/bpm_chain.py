# Run test

import numpy as np
from monitor import pickup
from components import cable
from components import combiner
from components import analog_filter as af
from components import antialiasing_filter as aaf
from components import amplifier
from components import adc
from signal_operations import signal_operations as sop
from signal_operations import replicator
from dsp import dsp

class ConfigTest:
    def __init__(self):
        self.bunch_c=pickup.ConfigBunch()
        self.pickup_c=pickup.ConfigPickup()
        self.cable_c=cable.ConfigCable()
        self.combiner_c=combiner.ConfigCombiner()
        self.analog_filter_c=af.ConfigAnalogFilter()
        self.antialiasing_filter_c=aaf.ConfigAAFilter()
        self.amplifier_c=amplifier.ConfigAmplifier()
        self.adc_c=adc.ConfigAdc()
        # Signal Level configuration
        self.calTol = 0.67
        self.worstCaseAmp = False
        self.fixAtt = False
        self.fixAttValue = 0.5 
        # DSP configuration
        self.interp = 1
        self.algorithm = 'rms' 
        self.electrodeLen = 10.5e-9
        self.triggerOffset = 2.2e-9
        # Test configuration
        self.replica_num = 5000
        
# Function to calculate attenuation/gain needed to reach calTol fraction of ADC input range with maximum signal bunch and given configuration conf
def attenuationCal(conf): 
    pu = pickup.pickup(conf.bunch_c, conf.pickup_c, filename="",maximum=True)
    puwc = cable.cable(pu, conf.cable_c)
    puwc = combiner.threeBunches(puwc, conf.combiner_c)
    combined = combiner.timeMux(puwc, conf.combiner_c, sigLen=3*(conf.combiner_c.bunch_dist+1e-9))
    filtered = af.analog_filter(combined, conf.analog_filter_c)
    if conf.amplifier_c.amp_active == True:
        att_preamp = (conf.amplifier_c.Vin_max)*conf.calTol/np.max(filtered[1:,:])
        if att_preamp > 1:
            att_preamp = 1
        Av = conf.amplifier_c.Av_min
        filtered[1:,:] = filtered[1:,:]*att_preamp*Av
    else:
        att_preamp = 1
    if conf.antialiasing_filter_c.aaf == True:
        filtered = aaf.AAFLPBessel(filtered,conf.antialiasing_filter_c)
    att_preADC=((conf.adc_c.P2P/2.)*conf.calTol)/np.max(filtered[1:,:])
    return att_preADC*att_preamp

# Function to compute the signal level along the chain
def fixAttCal(conf): 
    pu = pickup.pickup(conf.bunch_c, conf.pickup_c, filename="")
    puwc = cable.cable(pu, conf.cable_c)
    print("Amplitude Loss of the cable: %f dB" % (20*np.log10(np.max(puwc[1:,:])/np.max(pu[1:,:]))))
    print("Power Loss of the cable: %f dB\n" % (10*np.log10(np.sum(np.square(puwc[1,:]))/np.sum(np.square(pu[1,:])))))    
    combined = combiner.timeMux(puwc, conf.combiner_c, sigLen=(conf.bunchDist+1e-9))
    print("Amplitude Loss of the combiner: %f dB" % (20*np.log10(np.max(combined[1:,:])/np.max(puwc[1:,:]))))
    print("Power Loss of the combiner: %f dB\n" % (10*np.log10(np.sum(np.square(combined[1,:int(combined.shape[1]/2)]))/np.sum(np.square(puwc[1,:])))))
    filtered = af.analog_filter(combined, conf.analog_filter_c)
    print("Amplitude Loss of the filter: %f dB" % (20*np.log10(np.max(filtered[1:,:])/np.max(combined[1:,:]))))
    print("Power Loss of the filter: %f dB\n" % (10*np.log10(np.sum(np.square(filtered[1,:]))/np.sum(np.square(combiner[1,:])))))
    if conf.antialiasing_filter_c.aaf == True:
        aafiltered = aaf.AAFLPBessel(filtered,conf.antialiasing_filter_c)
        print("Amplitude Loss of the AAF filter: %f dB" % (20*np.log10(np.max(aafiltered[1:,:])/np.max(filtered[1:,:]))))
        print("Power Loss of the AAF filter: %f dB\n" % (10*np.log10(np.sum(np.square(aafiltered[1,:]))/np.sum(np.square(filtered[1,:])))))
    else:
        aafiltered = np.array(filtered)
    att_preADC=((conf.adc_c.P2P/2.)*conf.calTol)/np.max(aafiltered[1:,:])
    print("Amplitude Loss of the full acquisition chain: %f dB" % (20*np.log10(np.max(aafiltered[1:,:])/np.max(pu[1:,:]))))
    return att_preADC

# Function to generate master signal for bunch detection
def masterGen(conf): 
    pu_o = pickup.pickup(conf.bunch_c, conf.pickup_c, filename="")
    puwc = cable.cable(pu_o, conf.cable_c)
    filtered = af.analog_filter(puwc, conf.analog_filter_c)
    filtered = sop.extend(filtered, sigLen=14.5e-9)
    train = replicator.replicator(filtered, replica_num=1)
    if conf.antialiasing_filter_c.aaf == True:
        filtered = aaf.AAFLPBessel(filtered,conf.antialiasing_filter_c)
    adc_o=adc.adc(train, conf.adc_c)
    if conf.interp > 1:
        adc_o=sop.PUinterp(adc_o, interp=conf.interp)
    return adc_o
        
# Function to generate signals coherent to the configuration
def signalGen(conf, scopeMode = False): 
    pu_o = pickup.pickup(conf.bunch_c, conf.pickup_c, filename="")
    puwc = cable.cable(pu_o, conf.cable_c)
    puwc = combiner.threeBunches(puwc, conf.combiner_c)
    combined = combiner.timeMux(puwc, conf.combiner_c, sigLen=3*(conf.combiner_c.bunch_dist+1e-9))
    filtered = af.analog_filter(combined, conf.analog_filter_c)
    if conf.fixAtt == True:
        fixAtt=conf.fixAttValue
    else:
        fixAtt = attenuationCal(conf)
    if conf.worstCaseAmp:
        fixAtt = fixAtt*0.5
    filtered[1:,:] = filtered[1:,:]*fixAtt
    train = replicator.replicator(filtered, replica_num=conf.replica_num)
    if conf.amplifier_c.amp_active == True:
        Av = conf.amplifier_c.Av_min*conf.bunch_c.int_max/conf.bunch_c.intensity
        train = amplifier.amplifier(train, Av, conf.amplifier_c)
    if conf.antialiasing_filter_c.aaf == True:
        train = aaf.AAFLPBessel(train,conf.antialiasing_filter_c)
    adc_o=adc.adc(train, conf.adc_c)
    if conf.interp > 1:
        adc_o=sop.PUinterp(adc_o, interp=conf.interp)
    if scopeMode:
        signalOut = [pu_o,puwc,combined,filtered,adc_o]
    else:
        signalOut = adc_o
    return signalOut

# Main function 
def test(conf):
    factor=attenuationCal(conf)
    print(f"attenuation factor: {factor}")
    adc_o = signalGen(conf, scopeMode = False)
    print("signal generated")
    master = masterGen(conf)
    print("master generated")
    trigger = dsp.bunchDetector(matrix=adc_o[:,1:,:],master=master[0,1,:], electrodesPerSignal=conf.combiner_c.timeMuxN, bunchesPerTurn=3)
    pos=dsp.rmsSinglet(adc_o, trigger, conf.combiner_c, offset = conf.triggerOffset, electrodeLen = conf.electrodeLen)
    posmm=dsp.norm2mm(pos, r_button=conf.pickup_c.r_button, r_pipe=conf.pickup_c.r_pipe)
    errparam=dsp.errorEval(conf.bunch_c.x,conf.bunch_c.y,posmm)
    results=[posmm,errparam,factor,conf]
    return np.array(results,dtype=object)
