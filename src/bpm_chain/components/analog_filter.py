# Analogue Filter Block
import numpy as np
import signal_operations.signal_operations as sop
import scipy.signal


class ConfigAnalogFilter:
    def __init__(self):
        self.analog = 'comb' # 'lp' | 'comb'
        self.comb_rep = 4
        self.comb_delta = 2e-9
        self.comb_att = 0.98
        self.LP_Wn = 1.26e8
        self.LP_N = 4
    def set_analog(self, analog):
        self.analog = analog
    def set_LP(self, LP_Wn=1.26e8, LP_N=4):
        self.analog ='lp'
        self.LP_Wn = LP_Wn
        self.LP_N = LP_N
    def set_comb(self, comb_rep=4, comb_delta=2e-9, comb_att=0.98):
        self.analog = 'comb'
        self.comb_rep = comb_rep
        self.comb_delta = comb_delta
        self.comb_att = comb_att
    def show(self):
        print(f"Analog Filters Configuration:\
                \n\t Type of filter (analog) = {self.analog},")
        if self.analog=='comb':
            print(f"\t number of repetitions (comb_rep) = {self.comb_rep},")
            print(f"\t delay between repetitions (comb_delta) = {self.comb_delta},")
            print(f"\t attenuation of delay line (comb_att) = {self.comb_att},")
        else:
            print(f"\t LP filter bandwidth (LP_Wn) = {self.LP_Wn},\
                    \n\t LP filter order (LP_N) = {self.LP_N},")


# Combfilter

def comb(input_signal, analog_filter_c):
    ts = input_signal[0,1] - input_signal[0,0]
    div=2**(2*np.ceil(np.log2(analog_filter_c.comb_rep)))
    filter_i=np.array(input_signal[1:,:])
    deltasamples=int(analog_filter_c.comb_delta/ts)
    filter_o = np.zeros((input_signal.shape[0],filter_i.shape[1]+analog_filter_c.comb_rep*deltasamples))
    for i in range(0,analog_filter_c.comb_rep):
        filter_o[1:,i*deltasamples:i*deltasamples+filter_i.shape[1]] = filter_o[1:,i*deltasamples:i*deltasamples+filter_i.shape[1]] + filter_i*(analog_filter_c.comb_att**i)/div
    filter_o[0,:] = np.arange(input_signal[0,0], input_signal[0,0]+ts*filter_o.shape[1], ts)[0:len(filter_o[0,:])]
    return filter_o
    
# Lowpass filter

def LPBessel(input_signal, LP_N=4, LP_Wn=2e8):
    time=np.array(input_signal[0,:])
    signals=np.array(input_signal[1:,:])
    ts=time[1]-time[0]
    sos = scipy.signal.bessel(LP_N, 2*LP_Wn*ts, 'low', analog=False, output='sos')
    filtered=scipy.signal.sosfilt(sos, signals, axis=1)
    filtered_time=np.arange(0,ts*filtered.shape[1],ts)
    filter_o = np.zeros((input_signal.shape[0],filtered.shape[1]))
    filter_o[0,:] = filtered_time
    filter_o[1:,:]= filtered
    return filter_o


# Top function filter

def analog_filter(signal, analog_filter_c):
    if analog_filter_c.analog == 'lp':
        filtered = LPBessel(signal, analog_filter_c.LP_N, analog_filter_c.LP_Wn)
    elif analog_filter_c.analog == 'comb':
        filtered = comb(signal, analog_filter_c)
    else:
        print("WARNING: analog filter type not valid!")
        filtered = np.array(signal)
    return filtered

