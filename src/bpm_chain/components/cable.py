# Cable model
import numpy as np
import scipy.signal
import signal_operations.signal_operations as sop

class ConfigCable:
    def __init__(self):
        self.length = 64 # 64 | 100
        self.b = 1.2e-7 # 1.3e-7 
        self.v = 0.88 # 0.81
    def set_length(self, cblLen):
        if cblLen<15:
            print("WARNING: cable model not accurate for lengths minor than 15 m. Cable length set to 15 m.")
            cblLen=15
        self.length = cblLen
    def set_cblb(self, cblb):
        self.b = cblb
    def set_cblv(self, cblv):
        self.v = cblv
    def show(self):
        print(f"Cable Configuration: \
              \n\t length (length) = {self.length} m,\
              \n\t cable geometric parameter (b) = {self.b},\
              \n\t cable propagation velocity (v) = {self.v} c")

#NOTE: function should be improved for short cables (length < 15m), to be reviewed to consider reflections
        
def cable(signal_i, cable_c):
    signal = np.array(signal_i)
    signal = sop.extend(signal, 30e-9)
    v=cable_c.v*3e8
    tau=cable_c.length/v
    t=signal[0,:]+tau+signal[0,1]
    att=signal[0,1]-signal[0,0]
    cablepulse=(1/(np.sqrt(np.pi*2*(t-tau))*(t-tau)))*cable_c.b*cable_c.length*np.exp(-np.square(cable_c.b*cable_c.length)/(2*(t-tau)))*att
    signalwcable=np.array([scipy.signal.convolve(signal[item,:],cablepulse) for item in range(1,signal.shape[0])])
    signalwcable_o = np.zeros((signal.shape[0],signalwcable.shape[1]))
    signalwcable_o[1:,:] = signalwcable
    signalwcable_o[0,:] = np.arange(signal[0,0],signalwcable.shape[1]*(signal[0,1]-signal[0,0]),(signal[0,1]-signal[0,0]))
    signalwcable_o=sop.extend(signalwcable_o, 25e-9)
    return signalwcable_o