# Combiner Module
import numpy as np
import signal_operations.signal_operations as sop


class ConfigCombiner:
    def __init__(self):
        self.timeMuxN = 2
        self.delayAB = 12.5e-9
        self.delayHV = 6.25e-9
        self.splitter_att = 0.56
        self.delay_att = 0.9
        self.bunch_dist = 24.95e-9
    def set_timeMuxN(self, timeMuxN):
        self.timeMuxN = timeMuxN
    def set_delayAB(self, delayAB):
        self.delayAB = delayAB
    def set_delayHV(self, delayHV):
        self.delayHV = delayHV
    def set_splitter_att(self, splitter_att):
        self.splitter_att = splitter_att
    def set_delay_att(self, delay_att):
        self.delay_att = delay_att
    def set_bunch_dist(self, bunch_dist):
        self.bunch_dist = bunch_dist
    def show(self):
        print(f"Combiner Configuration: \
              \n\t Number of signals combined (timeMuxN) = {self.timeMuxN},\
              \n\t Delay between signal A and B (delayAB) = {self.delayAB} s,\
              \n\t Delay between signal A and C (delayHV) = {self.delayHV} s,\
              \n\t attenuation of the splitter (splitter_att) = {self.splitter_att},\
              \n\t attenuation of the delay line (delay_att) = {self.delay_att},\
              \n\t Time distance between bunches (bunch_dist) = {self.bunch_dist} s")

# Time Multiplexor

def threeBunches(signal, combiner_c, t0=2e-9):
    tB_o = sop.extend(signal, t0+3*combiner_c.bunch_dist)
    tB_o[1:,:] = tB_o[1:,:]+sop.delay(np.array(tB_o),combiner_c.bunch_dist)[1:,:]+sop.delay(np.array(tB_o),2*combiner_c.bunch_dist)[1:,:]
    return tB_o

    
def timeMux(pu, combiner_c, sigLen=25e-9):
    signal = sop.extend(pu, sigLen)
    if combiner_c.timeMuxN==1:
        timeMux_o = signal
    else:
        time = signal[0,:]
        signalHA = signal[0:2,:]*combiner_c.splitter_att
        signalHA[0,:] = np.array(time)
        signalVA = signal[0:4:3,:]*combiner_c.splitter_att
        signalVA[0,:] = np.array(time)
        signalHB = sop.delay(signal[0:3:2,:],combiner_c.delayAB)*combiner_c.splitter_att*combiner_c.delay_att
        signalHB[0,:] = np.array(time)
        signalVB = sop.delay(signal[0:5:4,:],combiner_c.delayAB)*combiner_c.splitter_att*combiner_c.delay_att
        signalVB[0,:] = np.array(time)
        signalH = signalHA[1,:] + signalHB[1,:]
        if combiner_c.timeMuxN==4:
            signalVA = sop.delay(np.array(signalVA),combiner_c.delayHV)*combiner_c.delay_att
            signalVB = sop.delay(np.array(signalVB),combiner_c.delayHV)*combiner_c.delay_att
            signalV = signalVA[1,:] + signalVB[1,:]
            timeMux_o = np.zeros((2,len(time)))
            timeMux_o[0,:] = time
            timeMux_o[1,:] = signalH[:] + signalV[:]
        else:
            signalV = signalVA[1,:] + signalVB[1,:]
            timeMux_o = np.zeros((3,len(time)))
            timeMux_o[0,:] = time
            timeMux_o[1,:] = signalH
            timeMux_o[2,:] = signalV        
    return timeMux_o