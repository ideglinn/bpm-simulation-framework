# Amplifier Block
import numpy as np

# Configuration Class
class ConfigAmplifier:
    def __init__(self):
        self.amp_active = True
        self.NF_dB = 6
        self.ND_dBm = -154
        self.R_L = 50
        self.Av_max=8.128
        self.Av_min=0.234
        self.Av_th=5.01
        self.Vin_max=7
        self.variable_gain =False
        self.mixed_arch = False
    def set_amp_active(self, amp_active):
        self.amp_active = amp_active
    def set_NF_dB(self, NF_dB):
        self.NF_dB = NF_dB
    def set_ND_dBm(self, ND_dBm):
        self.ND_dBm = ND_dBm
    def set_R_L(self, R_L):
        self.R_L = R_L
    def set_Av_max(self, Av_max):
        self.Av_max = Av_max
    def set_Av_min(self, Av_min):
        self.Av_min = Av_min
    def set_Av_th(self, Av_th):
        self.Av_th = Av_th
    def set_Vin_max(self, Vin_max):
        self.Vin_max = Vin_max
    def set_variable_gain(self, variable_gain):
        self.variable_gain = variable_gain
    def set_mixed_arch(self, mixed_arch):
        self.mixed_arch = mixed_arch
    def show(self):
        print(f"Amplifier Configuration: \
              \n\t amplifier is active (amp_active) = {self.amp_active},\
              \n\t noise figure (NF_dB) = {self.NF_dB} dB,\
              \n\t noise spectral density at the output (ND_dBm) = {self.ND_dBm} dBm,\
              \n\t maximum gain (Av_max) = {self.Av_max},\
              \n\t minimum gain (Av_min) = {self.Av_min},\
              \n\t threshold gain in mixed architecture to activate active amplification (Av_th) = {self.Av_th},\
              \n\t maximum signal level at the input (Vin_max) = {self.Vin_max} V,\
              \n\t active amplification is available (variable_gain) = {self.variable_gain},\
              \n\t mixed architecture passive/active is available (mixed_arch) = {self.mixed_arch}")

def amplifier_singleBunch(signal_i, Av, amplifier_c, T = 298.15):
    if np.max(signal_i[1:,:])>amplifier_c.Vin_max:
        print('### WARNING Amplifier: Input Saturation ###')
    if Av<amplifier_c.Av_min:
        Av=Av_min
        print('### WARNING Amplifier: Minimum amplification reached ###')
    elif Av>amplifier_c.Av_max:
        Av=amplifier_c.Av_max
        print('### WARNING Amplifier: Maximum amplification reached ###')
    bw = 1/(2*(signal_i[0,1]-signal_i[0,0]))
    if amplifier_c.mixed_arch:
        if Av < amplifier_c.Av_th:
            amplifier_c.variable_gain = False
        else:
            amplifier_c.variable_gain = True
    if amplifier_c.variable_gain:
        kb = 1.380649e-23
        Av_dB = 20*np.log10(Av)
        nd_i = 10*np.log10(kb*T*1e3)
        nd_o = nd_i + Av_dB + amplifier_c.NF_dB
    else:
        nd_o = amplifier_c.ND_dBm
    nStd = np.sqrt(10**(nd_o/10)*bw*1e-3*amplifier_c.R_L)
    noise_o = np.array(np.random.normal(scale=nStd, size=(np.shape(signal_i[1:,:]))))
    signal_o=np.array(signal_i)
    signal_o[1:,:] = Av*np.array(signal_i[1:,:])+noise_o
    return signal_o

# NOTE: the only difference between this function and the other is the dimension of "signal"
def amplifier(signal_i, Av, amplifier_c, T = 298.15):
    if np.max(signal_i[0,1:,:])>amplifier_c.Vin_max:
        print('### WARNING Amplifier: Input Saturation ###')
    if Av<amplifier_c.Av_min:
        Av=amplifier_c.Av_min
        print('### WARNING Amplifier: Minimum amplification reached ###')
    elif Av>amplifier_c.Av_max:
        Av=amplifier_c.Av_max
        print('### WARNING Amplifier: Maximum amplification reached ###')
    bw = 1/(2*(signal_i[0,0,1]-signal_i[0,0,0]))
    kb = 1.380649e-23
    Av_dB = 20*np.log10(Av)
    nd_i = 10*np.log10(kb*T*1e3)
    if amplifier_c.mixed_arch:
        if Av < amplifier_c.Av_th:
            amplifier_c.variable_gain = False
        else:
            amplifier_c.variable_gain = True
    if amplifier_c.variable_gain:
        nd_o = nd_i + Av_dB + amplifier_c.NF_dB
    else:
        nd_o = amplifier_c.ND_dBm
    nStd = np.sqrt(10**(nd_o/10)*bw*1e-3*amplifier_c.R_L)
    noise_o = np.array(np.random.normal(scale=nStd, size=(np.shape(signal_i)[0],np.shape(signal_i[:,1:,:])[1],np.shape(signal_i)[2])))
    signal_o=np.array(signal_i)
    signal_o[:,1:,:] = Av*np.array(signal_i[:,1:,:])+noise_o
    return signal_o