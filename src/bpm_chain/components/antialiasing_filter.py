# Anti-aliasing Filter Block
import numpy as np
import signal_operations.signal_operations as sop
from components import analog_filter
import scipy.signal


class ConfigAAFilter:
    def __init__(self):
        self.aaf = False
        self.aafBw = 500e6
        self.aafN = 8
    def set_aaf(self, aaf, aafBw=None, aafN=8):
        self.aaf = aaf
        self.aafBw = aafBw
        self.aafN = aafN
    def show(self):
        print(f"Antialiasing Filters Configuration:\
                \n\t Anti Aliasing Filter is active = {self.aaf}\
                \n\t Anti Aliasing Filter bandwidth (aafBw) = {self.aafBw},\
                \n\t Anti Aliasing Filter order (aafN) = {self.aafN},")



def antialiasing(signal_i, aafilter_c):
    signalSP = sop.PUfftTrain(signal_i)
    freq=signalSP[0,0,:]
    f0 = 0.1567 #bandwidth at -30 dB
    f1 = aafilter_c.aafBw
    # 74.10.10
    z = [0.1013388717e1,0.1205941773e1]
    p = [-0.5620313111, -0.5112124454+0.3921559882*1j, -0.3397647460+0.768389130*1j, -0.7881502523e-2+0.9391165825*1j]
    k = 0.9725771633e-1
    omega = (freq*2j*np.pi)*f0/f1
    G = (np.square(omega)+np.square(z[0]))*(np.square(omega)+np.square(z[1]))
    P = ((omega-p[0])*(omega-p[1])*(omega-np.conj(p[1]))*(omega-p[2])*(omega-np.conj(p[2]))*(omega-p[3])*(omega-np.conj(p[3])))
    h = k*np.divide(G,P) 
    signalSP[:,1:,:] = signalSP[:,1:,:] * h
    signal_out = np.fft.irfft(signalSP[:,1:,:], axis=2)
    signal_o = np.zeros((signal_i.shape[0],signal_i.shape[1],signal_out.shape[2]))
    signal_o[:,0,:] = signal_i[0,0,0:signal_out.shape[2]]
    signal_o[:,1:,:]= signal_out
    return signal_o
    
    
def AAFLPBessel(signal_i, aafilter_c):
    if signal_i.ndim >2: 
        time=np.array(signal_i[0,0,:])
        signals=np.array(signal_i[:,1:,:])
        ts=time[1]-time[0]
        sos = scipy.signal.bessel(aafilter_c.aafN, 2*aafilter_c.aafBw*ts, 'low', analog=False, output='sos')
        filtered=scipy.signal.sosfilt(sos, signals, axis=2)
        filtered_time=np.arange(0,ts*filtered.shape[2],ts)
        signal_o = np.zeros((signal_i.shape[0],signal_i.shape[1],filtered.shape[2]))
        signal_o[:,0,:] = filtered_time
        signal_o[:,1:,:]= filtered
    else:
        signal_o = analog_filter.LPBessel(signal_i, aafilter_c.aafN, aafilter_c.aafBw)
    return signal_o

