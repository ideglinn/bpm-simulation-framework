# Analog to Digital Converter Block

import numpy as np

class AdcType:
    def __init__(self, name, fs, P2P, bits, enobs):
        self.name=name
        self.fs=fs
        self.P2P = P2P
        self.bits = bits
        self.enobs = enobs        

htg_t = AdcType(name='AD9208',fs=26e8, P2P=2.04, bits=14, enobs=9.6)
vadatech_t = AdcType(name='TIADC12J4000',fs=32e8, P2P=0.725, bits=12, enobs=8.8)
slow_t = AdcType(name="slow",fs=5e8, P2P=1.9, bits=16, enobs=11.6)
teledyne_t = AdcType(name="Teledyne",fs=5e9, P2P=1, bits=12, enobs=8.1)
manual_t = AdcType(name="manual",fs=None, P2P=None, bits=None, enobs=None)
        
# Configuration class
class ConfigAdc:
    def __init__(self, adctype=manual_t):
        self.adctype = adctype
        self.fs = adctype.fs
        self.P2P = adctype.P2P
        self.bits = adctype.bits
        self.enobs = adctype.enobs
        self.mode = 'asynch' # 'asynch' | 'synch' | 'linear'
    def set_adctype(self, adctype):
        self.adctype=adctype
        self.fs = adctype.fs
        self.P2P = adctype.P2P
        self.bits = adctype.bits
        self.enobs = adctype.enobs
    def set_fs(self, fs):
        self.adctype = manual_t
        self.fs = fs
    def set_P2P(self, P2P):
        self.adctype = manual_t
        self.P2P = P2P
    def set_bits(self, bits):
        self.adctype = manual_t
        self.bits = bits
    def set_enobs(self, enobs):
        self.adctype = manual_t
        self.enobs = enobs
    def set_mode(self, mode):
        self.mode = mode
    def show(self):
        print(f"ADC Configuration: \
              \n\t configuration type (adctype) = {self.adctype.name},\
              \n\t sampling rate (fs) = {self.fs*1e-9} Gsps,\
              \n\t input range peak to peak = {self.P2P} V,\
              \n\t number of bits (bits) = {self.bits},\
              \n\t effective number of bits (enobs) = {self.enobs},\
              \n\t sampling phase (mode) = {self.mode}")


# Specific Functions
def resample(signal_i, fs=3e9, mode='asynch'):
    t_i=signal_i[0,0,1]-signal_i[0,0,0]
    ts = int(1/(t_i*fs))
    if mode=='synch':
        toffset = np.array(np.zeros((signal_i.shape[0],signal_i.shape[1],1)), dtype=np.int)        
    elif mode=='linear':
        toffsetvalues = np.array(np.arange(0,signal_i.shape[0]), dtype=np.int)
        toffset = np.array(np.ones((signal_i.shape[0],signal_i.shape[1],1)), dtype=np.int)
        toffset = np.transpose(toffsetvalues*np.transpose(toffset))
    else:
        toffset = np.array(np.random.rand(signal_i.shape[0],signal_i.shape[1],1)*ts, dtype=np.int)
    samples = np.int(np.floor((signal_i.shape[2]-np.max(toffset))/ts))
    signal_o = np.zeros((signal_i.shape[0],signal_i.shape[1],samples))
    for i in range(0,signal_i.shape[0]):
        for j in range(0,signal_i.shape[1]):
            signal_o[i,j] = signal_i[i,j,int(toffset[i,j]):int(toffset[i,j]+samples*ts):ts]
    return signal_o

def encode(signal_i, P2Prange=5, bits=12): 
    delta=P2Prange*2**(-bits)
    signal_o = np.array(signal_i)
    signal_o[:,1:,:] = np.array(signal_i[:,1:,:]/delta, dtype=np.int)
    if np.max(signal_i[:,1:,:]) > P2Prange/2.:
        print('### WARNING: ADC Over range ###')
    return signal_o
        
def bins2Volts(signal_i, P2Prange=5, bits=12): 
    delta=P2Prange*2**(-bits)
    signal_o = np.array(signal_i)
    signal_o[:,1:,:] = signal_o[:,1:,:]*delta
    return signal_o 
  
def addnoise(signal_i, bits=12, enobs=8.8, P2Prange=5):
    noisestd = np.sqrt(np.square(P2Prange*(2**(-enobs))/(np.sqrt(12)))-np.square(P2Prange*(2**(-bits))/(np.sqrt(12))))
    # normal distribution
    noise = np.array((np.random.normal(scale=noisestd, size=(np.shape(signal_i)[0],np.shape(signal_i[:,1:,:])[1],np.shape(signal_i)[2]))))
    # uniform distribution
    # noise = np.array((np.ones(np.shape(signal_i[:,1:,:]))*(-0.5)+np.random.rand(np.shape(signal_i)[0],np.shape(signal_i[:,1:,:])[1],np.shape(signal_i)[2]))*(2**noisebits), dtype=np.int)
    signal_o=np.array(signal_i)
    signal_o[:,1:,:] = np.array(signal_i[:,1:,:]+noise)
    return signal_o
    

    
#Full ADC chain
  
def adc(signal_i, adc_c, Volt=True):
    signal_o = resample(signal_i, fs=adc_c.fs, mode=adc_c.mode)
    signal_o = addnoise(signal_o, bits=adc_c.bits, enobs=adc_c.enobs, P2Prange=adc_c.P2P)
    signal_o = encode(signal_o, P2Prange=adc_c.P2P, bits=adc_c.bits)
    if Volt:
        signal_o = bins2Volts(signal_o, P2Prange=adc_c.P2P, bits=adc_c.bits)
    return signal_o
