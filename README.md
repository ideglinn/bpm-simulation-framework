# BPM Simulation Framework

This package provides modules to simulate the signal processing chain for a Beam Position Monitor.

In the _src/bpm_chain_ folder it contains the following subpackages.

### monitor/pickup
The subpackage _pickup_ contains the _pickup_ module, which generates single bunch pickup signals in function of the pickup type and geometry, and in function of the beam bunch intensity and position. The pickup module contains the configuration classes fot the bunch and for the pickup.

### components
The subpackage _components_ contains the modules simulating the response of different components that can be included in the BPM signal processing chain. These modules are _cable_, _combiner_, _analog_filter_, _amplifier_ and _adc_. Each component has their configuration class defined within the module.

### signal_operations
The subpackage _signal_operations_ contains the module _signal_operations_ which defines common used functions to manipulates the signals.

### dsp
The subpackage _dsp_ contains the module _dsp_ which defines the digital signal processing functions applied on the digitised data.

### bpm_chain
The module _bpm_chain_ contains the definition of the BPM chain configuration class, calling also the submodules classes, and the definition of the functions to build the processing chain out of the selected components and simulate the measured position results and statistic characteristics.

The _src/bpm_chain_ folder contains also the Jupyter notebook _demo.ipynb_ that shows examples of how configuring and using the package.

The folder _src/data_ stores the files for the generation of waveforms that can be used in the framework, e.g. the CST simulation of the waveform produced by a button pickup.

The folder _docs_ contains a document introducing Beam Position Monitors and the description of the main functions and modules.

